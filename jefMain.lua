--[[
  THIS CODE IS DEVELOPED AND MAINTAINED BY TheDwarfer

  DISCLAIMER:
    YOU ARE FREE TO COPY, DISTRIBUTE AND ALTER THE CODE WITH EXCEPTION
    FROM THIS DISCLAIMER. THE FOLLOWING CODE PROVIDES A SYSTEM FOR PLAYERS
    TO MONITOR AND INTERACT WITH MEKANISMS FISSION REACTOR.

  MODIFYING THE CODE FOR MALICIOUS INTENT IS NOT CONDONED AND WILL
  NOT HAVE ANY ASSOSCIATION WITH THE ORIGINAL VERSION OR ITS DEVELOPER

  Have fun, try not to blow it up. -TheDwarfer
]]

local modemChannel = 1
local expectedTime = 1
local monitorNameMode = false
local shutdown = false

local bigfont = require("bigfont")
local jefreactor = require("jefReactor")
local jefboiler = require("jefBoiler")
local jefturbine = require("jefTurbine")
local jefinduction = require("jefInduction")
local jefconfig = require("jefConfig")

-- Used to display errors or information during startup
-- (doesn't require modem like the other monitors)
local mainMonitor = peripheral.find("monitor")

-- Find the wired modems
local wiredModems = table.pack(peripheral.find("modem", function(_, modem)
  return not modem.isWireless()
end))

-- Find the wireless modems
local wirelessModems = table.pack(peripheral.find("modem", function(_, modem)
  return modem.isWireless()
end))

-- Setup side monitors
local sideMonitors = {}
local sideMonitorNames = {}

if jefconfig.enableReactor then
  sideMonitorNames["monitorReactorRuntime"] = jefconfig.monitorReactorRuntime
  sideMonitorNames["monitorReactorSetup"] = jefconfig.monitorReactorSetup
end
if jefconfig.enableReactor then
  sideMonitorNames["monitorReactorECS"] = jefconfig.monitorReactorECS
end
if jefconfig.enableBoiler then
  sideMonitorNames["monitorBoilerMain"] = jefconfig.monitorBoilerMain
  sideMonitorNames["monitorBoilerRuntime"] = jefconfig.monitorBoilerRuntime
  sideMonitorNames["monitorBoilerSetup"] = jefconfig.monitorBoilerSetup
end
if jefconfig.enableTurbine then
  sideMonitorNames["monitorTurbineMain"] = jefconfig.monitorTurbineMain
  sideMonitorNames["monitorTurbineRuntime"] = jefconfig.monitorTurbineRuntime
  sideMonitorNames["monitorTurbineSetup"] = jefconfig.monitorTurbineSetup
end
if jefconfig.enableInduction then
  sideMonitorNames["monitorInductionMain"] = jefconfig.monitorInductionMain
end

-- Prepare main monitor
mainMonitor.setBackgroundColor(colors.black)
mainMonitor.setTextScale(0.5)
mainMonitor.clear()

-- Timer variables
local lastTimeReactor = os.clock()
local lastTimeBoiler = os.clock()
local lastTimeTurbine = os.clock()
local lastTimeInduction = os.clock()

-- JEF Version
local JEFversion = "JEF V.0.8.1"
local JEFslogan = "\"My Name A JEF\""
local JEFcolor = "e14dba2d41a"



local function clearMonitors()
  -- Display the monitors names if it failed to fetch a monitor with given names
  local tempMonitors = wiredModems[1].getNamesRemote()
  mainMonitor.clear()
  for k, v in pairs(tempMonitors) do
    local monitor = peripheral.wrap(v)
    monitor.clear()
  end
end



local function displayMonitorNumbers()
  -- Display the monitors names if it failed to fetch a monitor with given names
  if monitorNameMode then
    local tempMonitors = wiredModems[1].getNamesRemote()
    for k, v in pairs(tempMonitors) do
      local monitor = peripheral.wrap(v)
      monitor.clear()
      bigfont.writeOn(monitor, 1, v, nil, nil)
    end
  end
end



local function setupMonitors()
  local status = true
  for k, v in pairs(sideMonitorNames) do
    local monitor = peripheral.wrap(tostring(v))
    if monitor then
      -- Ran ok so error is the wrap
      monitor.setBackgroundColor(colors.black)
      monitor.setTextScale(0.5)
      monitor.clear()
      sideMonitors[k] = monitor
    else
      monitorNameMode = true
      status = false
      print("Failed to get monitor: " .. v)
    end
  end

  return status
end



local function listenForMessage()
  while true do
    local event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
    if channel == modemChannel and not monitorNameMode and message then
      local listenerType = message["type"]

      -- Call the methods of each jef module (only 1 reactor so message is sent directly)
      if listenerType == "reactor" then
        -- Update timer
        lastTimeReactor = os.clock()

        local status, err = pcall(function()
          if jefconfig.enableReactor then
            -- Draw the main monitor
            jefreactor.drawMainMonitor(mainMonitor, message[1], lastTimeReactor, expectedTime)

            -- Draw timer
            jefreactor.drawTimer(mainMonitor, lastTimeReactor, expectedTime)

            -- Draw the runtime info monitor
            jefreactor.drawRuntimeInfoMonitor(sideMonitors["monitorReactorRuntime"], message[1])

            -- Draw the setup info monitor
            jefreactor.drawSetupInfoMonitor(sideMonitors["monitorReactorSetup"], message[1], JEFversion, JEFcolor, JEFslogan)
          end
          if jefconfig.enableECS then
            -- Draw the emergency control system monitor
            jefreactor.drawECSMonitor(sideMonitors["monitorReactorECS"], message[1])

            -- Run an ECS check
            jefreactor.ECScheck(message[1], wirelessModems[1], modemChannel)
          end
        end)
        if not status then
          print(err)
        end
      end

      if listenerType == "boiler" and jefconfig.enableBoiler then
        -- Update timer
        lastTimeBoiler = os.clock()

        local status, err = pcall(function()
          -- Draw the main monitor
          jefboiler.drawMainMonitor(sideMonitors["monitorBoilerMain"], message, lastTimeBoiler, expectedTime)

          -- Draw timer
          jefboiler.drawTimer(sideMonitors["monitorBoilerMain"], lastTimeBoiler, expectedTime)

          -- Draw Runtime Information
          jefboiler.drawRuntimeInfoMonitor(sideMonitors["monitorBoilerRuntime"], message)

          -- Draw the setup info monitor
          jefboiler.drawSetupInfoMonitor(sideMonitors["monitorBoilerSetup"], message)
        end)
        if not status then
          print(err)
        end
      end

      if listenerType == "turbine" and jefconfig.enableTurbine then
        -- Update timer
        lastTimeTurbine = os.clock()

        local status, err = pcall(function()
          -- Draw the main monitor
          jefturbine.drawMainMonitor(sideMonitors["monitorTurbineMain"], message, lastTimeTurbine, expectedTime)

          -- Draw timer
          jefturbine.drawTimer(sideMonitors["monitorTurbineMain"], lastTimeTurbine, expectedTime)

          -- Draw Runtime Information
          jefturbine.drawRuntimeInfoMonitor(sideMonitors["monitorTurbineRuntime"], message)

          -- Draw the setup info monitor
          jefturbine.drawSetupInfoMonitor(sideMonitors["monitorTurbineSetup"], message)
        end)
        if not status then
          print(err)
        end
      end

      if listenerType == "induction" and jefconfig.enableInduction then
        -- Update timer
        lastTimeInduction = os.clock()

        local status, err = pcall(function()
          -- Draw the main monitor
          jefinduction.drawMainMonitor(sideMonitors["monitorInductionMain"], message, lastTimeInduction, expectedTime)

          -- Draw timer
          jefinduction.drawTimer(sideMonitors["monitorInductionMain"], lastTimeInduction, expectedTime)
        end)
        if not status then
          print(err)
        end
      end
    end

    if shutdown then
      return nil
    end
  end
end



local function input()
  local termWidth, termHeight= term.getSize()

  while true do
    -- Show JEF message prompt
    term.clear()
    term.setCursorPos((termWidth-#JEFversion)/2, 1)
    term.write(JEFversion)
    term.setCursorPos((termWidth-#JEFslogan)/2, 2)
    term.write(JEFslogan)
    term.setCursorPos(1, termHeight)

    -- Setup sending message table
    message = {}

    -- Handle input
    local inputString = io.read()

    if inputString == "setup monitor" then
      monitorNameMode = not monitorNameMode
    end

    if inputString == "exit" then
      print("Shutting down reactor due to safety... ")
      message["header"] = "reactor"
      message["context"] = "stop"
      wirelessModems[1].transmit(modemChannel, modemChannel, message)
      print("Exiting...")
      shutdown = true
    end

    if inputString == "start" then
      message["header"] = "reactor"
      message["context"] = "start"
      wirelessModems[1].transmit(modemChannel, modemChannel, message)
    end

    if inputString == "stop" then
      message["header"] = "reactor"
      message["context"] = "stop"
      wirelessModems[1].transmit(modemChannel, modemChannel, message)
    end

    if inputString == string.match(inputString, "burnrate %d.+") then
      message["header"] = "reactor"
      message["context"] = inputString
      wirelessModems[1].transmit(modemChannel, modemChannel, message)
    end

    --sleep(5)

    if shutdown then
      return nil
    end
  end
end



-- Update Timers
local function tick()
  while true do
    if jefconfig.enableReactor then jefreactor.drawTimer(mainMonitor, lastTimeReactor, expectedTime) end
    if jefconfig.enableBoiler then jefboiler.drawTimer(sideMonitors["monitorBoilerMain"], lastTimeBoiler, expectedTime) end
    if jefconfig.enableTurbine then jefturbine.drawTimer(sideMonitors["monitorTurbineMain"], lastTimeTurbine, expectedTime) end
    if jefconfig.enableInduction then jefinduction.drawTimer(sideMonitors["monitorInductionMain"], lastTimeInduction, expectedTime) end
    sleep(1)

    if monitorNameMode then
      displayMonitorNumbers()
    end

    if shutdown then
      return nil
    end
  end
end



local function main()
  lastTimeReactor = os.clock()
  lastTimeBoiler = os.clock()
  lastTimeTurbine = os.clock()
  lastTimeInduction = os.clock()
  status = setupMonitors()

  if status then
    wirelessModems[1].open(modemChannel)

    -- Show Waiting for signal on main monitor
    bigfont.writeOn(mainMonitor, 1, "Waiting For Signal...", nil, nil)

    -- Run main methods in parallel
    parallel.waitForAny(tick, input, listenForMessage)

    -- Clear all monitors
    clearMonitors()

    -- Will only run if all parallel functions have returned/shutdown
    print("Completed exit, have a nice day!")
  else
    print("Please Review jefConfig File")
    displayMonitorNumbers()
  end
end

main()
