-- init
b = {}

local bigfont = require("bigfont")
local jefconfig = require("jefConfig")

b.drawPercentageBar = function(choosenMonitor, percentage, x, y, height, color, label)
  -- Draw background grey bar
  for i=y, y-height, -1 do
    choosenMonitor.setCursorPos(x, i)
    choosenMonitor.blit("\149\149\149\149", "7777", "7777")
  end

  -- Draw foreground bar (skip if < 1%)
  if percentage > 0.01 and percentage <= 1 then
    for i=y, y-math.floor(height*percentage), -1 do
      choosenMonitor.setCursorPos(x, i)
      choosenMonitor.blit("\149\149\149\149", color, color)
    end
  end

  -- Draw label
  bigfont.writeOn(choosenMonitor, 1, label, x-#label, y+3)
end



b.drawBoiler = function(choosenMonitor, x, y, data, name)
  local width = 51
  local steamHeight = 8
  local waterHeight = 16
  local waterColor = "b"
  local steamColor = "8"

  -- Draw title
  local boilerTitleUnderline = string.rep("\131", #name*3)
  bigfont.writeOn(choosenMonitor, 2, name, x + ((width - #name*9)/2), 2)
  bigfont.writeOn(choosenMonitor, 1, boilerTitleUnderline, x + ((width - #boilerTitleUnderline*3)/2), 10)

  -- Don't render steam if not active
  if data["status"] == "Inactive" then
    steamColor = "f"
  end

  -- Draw steam section
  for i=y, y+steamHeight do
    choosenMonitor.setCursorPos(x, i)
    choosenMonitor.blit(string.rep("\158", width), "0" .. string.rep(steamColor, width-2) .. "0", "0" .. string.rep("f", width-2) .. "0")
  end

 -- Draw water section
  for i=y+steamHeight, y+steamHeight+waterHeight do
    choosenMonitor.setCursorPos(x, i)
    choosenMonitor.blit(string.rep(" ", width), "0" .. string.rep(waterColor, width-2) .. "0", "0" .. string.rep(waterColor, width-2) .. "0")
  end

  -- Draw the horizontal lines
  local hLine = string.rep(" ", width)
  choosenMonitor.setCursorPos(x,y)
  choosenMonitor.blit(hLine, string.rep("0", width), string.rep("0", width))
  choosenMonitor.setCursorPos(x,y+steamHeight)
  choosenMonitor.blit(hLine, string.rep("0", width), string.rep("0", width))
  choosenMonitor.setCursorPos(x,y+steamHeight+waterHeight)
  choosenMonitor.blit(hLine, string.rep("0", width), string.rep("0", width))

  -- Draw temp and boilrate
  local temp = math.floor(data["temperature"]) .. " K"
  local boilRate = math.floor(data["lastBoilRate"]) .. " mB/t"
  bigfont.blitOn(choosenMonitor, 1, temp, string.rep("0", #temp), string.rep(waterColor, #temp), x + ((width - #temp*3)/2), y+steamHeight+10)
  bigfont.blitOn(choosenMonitor, 1, boilRate, string.rep("0", #boilRate), string.rep(waterColor, #boilRate), x + ((width - #boilRate*3)/2), y+steamHeight+5)


  -- Draw status
  local boilerStatus = "Status: " .. data["status"]
  if boilerStatus == "Status: Active" then
    boilerStatusX = x + 5
  elseif boilerStatus == "Status: Inactive" then
    boilerStatusX = x + 2
  end

  bigfont.blitOn(choosenMonitor, 1, boilerStatus, string.rep("5", #boilerStatus), string.rep("f", #boilerStatus), boilerStatusX, 40)
end



b.drawTurbine = function(choosenMonitor, x, y, width, height, data, name)
  local waterHeight = math.floor(height*0.33)
  local steamHeight = math.floor(height*0.67)
  local bladeSpacing = 4
  local bladeIncrease = 1
  local bladeMax = width - 5
  local waterColor = "b"
  local steamColor = "8"

  -- Draw title
  local boilerTitleUnderline = string.rep("\131", #name*3)
  bigfont.writeOn(choosenMonitor, 2, name, x + ((width - #name*9)/2), 2)
  bigfont.writeOn(choosenMonitor, 1, boilerTitleUnderline, x + ((width - #boilerTitleUnderline*3)/2), 10)

  -- Don't render steam if not active
  if data["status"] == "Inactive" then
    steamColor = "f"
  end

  -- Draw water section
  for i=y, y+waterHeight do
    choosenMonitor.setCursorPos(x, i)
    choosenMonitor.blit(string.rep(" ", width), "0" .. string.rep(waterColor, width-2) .. "0", "0" .. string.rep("f", width-2) .. "0")
  end

  -- Draw steam section
  for i=y+waterHeight, y+waterHeight+steamHeight do
   choosenMonitor.setCursorPos(x, i)
   local stringColor = ""
   local bladeLength = bladeMax - (bladeIncrease*(i - (y+waterHeight))) + 3 -- Change this value if crashing

   if math.floor(i % bladeSpacing) == 0 and bladeLength >= 3 then
     stringColor = "0" .. string.rep(steamColor, math.floor(width/2)-1-math.floor(bladeLength/2)) .. string.rep("0", bladeLength) .. string.rep(steamColor, math.floor(width/2)-1-math.floor(bladeLength/2)) .. "0"
   else
     stringColor = "0" .. string.rep(steamColor, math.floor(width/2)-1) .. "0" .. string.rep(steamColor, math.floor(width/2)-1) .. "0"
   end

   --print("B: " .. bladeLength)
   --print("S: " .. #stringColor)
   --print("W: " .. width)
   choosenMonitor.blit(string.rep(" ", width), stringColor, stringColor)
  end

  -- Draw the horizontal lines
  local hLine = string.rep(" ", width)
  choosenMonitor.setCursorPos(x,y)
  choosenMonitor.blit(hLine, string.rep("0", width), string.rep("0", width))
  choosenMonitor.setCursorPos(x,y+waterHeight)
  choosenMonitor.blit(hLine, string.rep("0", width), string.rep("0", width))
  choosenMonitor.setCursorPos(x,y+waterHeight+steamHeight)
  choosenMonitor.blit(hLine, string.rep("0", width), string.rep("0", width))

  -- Draw status & dump mode
  local turbineStatus = data["status"]
  if turbineStatus == "Active" then
    turbineStatusX = x + 6
  elseif turbineStatus == "Inactive" then
    turbineStatusX = x + 3
  end

  bigfont.blitOn(choosenMonitor, 1, data["dumpingMode"], string.rep("5", #data["dumpingMode"]), string.rep("f", #data["dumpingMode"]), x + ((width - #data["dumpingMode"]*3)/2), 62)
  bigfont.blitOn(choosenMonitor, 1, turbineStatus, string.rep("5", #turbineStatus), string.rep("f", #turbineStatus), turbineStatusX, 59)
end



b.drawInduction = function(choosenMonitor, x, y, width, height, message)
  local sideWidth = width*0.15
  local sideOffset = 5
  local hLine = string.rep(" ", width)

  for i=y, y+height do
    local inputColor = "5"
    local outputColor = "e"

    if message["lastInput"] < 1 then
      inputColor = "f"
    end
    if message["lastOutput"] < 1 then
      outputColor = "f"
    end

    local lineColor = "0" .. string.rep(inputColor, sideWidth) .. "0" .. string.rep("f", width-(2*sideWidth)-4) .. "0" .. string.rep(outputColor, sideWidth) .. "0"
    choosenMonitor.setCursorPos(x,i)
    choosenMonitor.blit(hLine, lineColor, lineColor)
  end

  -- Draw the horizontal lines
  choosenMonitor.setCursorPos(x,y)
  choosenMonitor.blit(hLine, string.rep("0", width), string.rep("0", width))
  choosenMonitor.setCursorPos(x,y+height)
  choosenMonitor.blit(hLine, string.rep("0", width), string.rep("0", width))

  -- Draw energy value inside and on sides
  local energyString = math.floor(message["energy"]/1000000 * jefconfig.energyTable[jefconfig.inductionEnergy]) .. " M" .. jefconfig.inductionEnergy
  local energyCapString = math.floor(message["energyCap"]/1000000 * jefconfig.energyTable[jefconfig.inductionEnergy]) .. " M" .. jefconfig.inductionEnergy
  local inputString = math.floor(message["lastInput"]/1000 * jefconfig.energyTable[jefconfig.inductionEnergy]) .. " K" .. jefconfig.inductionEnergy
  local outputString = math.floor(message["lastOutput"]/1000 * jefconfig.energyTable[jefconfig.inductionEnergy]) .. " K" .. jefconfig.inductionEnergy 
  bigfont.blitOn(choosenMonitor, 1, energyString, string.rep("0", #energyString), string.rep("f", #energyString), x + ((width - #energyString*3)/2), y + (height/2))
  bigfont.blitOn(choosenMonitor, 1, energyCapString, string.rep("0", #energyCapString), string.rep("f", #energyCapString), x + ((width - #energyCapString*3)/2), y - sideOffset)
  bigfont.blitOn(choosenMonitor, 1, inputString, string.rep("5", #inputString), string.rep("f", #inputString), x - sideOffset - (#inputString*3), y + (height/2))
  bigfont.blitOn(choosenMonitor, 1, outputString, string.rep("e", #outputString), string.rep("f", #outputString), x + width + sideOffset, y + (height/2))
end



-- Adjust string to be size required (adding spaces)
b.adjustStringSize = function(stringInput, size, side)
  if #stringInput == size then
    return stringInput
  end

  while #stringInput < size do
    if side == "left" then
      stringInput = " " .. stringInput
    elseif side == "right" then
      stringInput = stringInput .. " "
    end
  end
  return stringInput
end

return b
