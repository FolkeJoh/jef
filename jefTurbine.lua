-- Init
local b = {}
local bigfont = require("bigfont")
local jefutils = require("jefUtils")
local jefconfig = require("jefConfig")

-- Default Values
local timerX = 2
local timerY = 67

-- config Values

b.drawTimer = function(choosenMonitor, lastTime, expectedTime)
    local timeDif = math.floor(os.clock()-lastTime)
    local timerString = "Last Update: " .. timeDif .. " Seconds"
    local timerColor = string.rep("d", #timerString)  -- Default green

    if timeDif-1 > expectedTime then              -- Turn orange
      timerColor = string.rep("1", #timerString)
    end
    if timeDif > expectedTime*2+1 then           -- Trun red
      timerColor = string.rep("e", #timerString)
    end

    choosenMonitor.setCursorPos(timerX, timerY)
    choosenMonitor.blit(timerString, timerColor, string.rep("f", #timerString))
end



b.drawMainMonitor = function(choosenMonitor, message, lastTime, expectedTime)
  -- For placement
  local monitorWidth, monitorHeight = choosenMonitor.getSize()

  -- Clear monitor
  choosenMonitor.clear()

  for k, turbine in pairs(message) do
    -- Ignore the message type key and first value
    if type(k) == "number" then
      jefutils.drawTurbine(choosenMonitor, 10 + ((k-1)*57), 15, 31, 42, turbine, "T." .. k)   -- Width has to be odd
    end
  end
end



b.drawRuntimeInfoMonitor = function(choosenMonitor, message)
  choosenMonitor.clear()

  local width, height = choosenMonitor.getSize()
  local energyType = jefconfig.turbineEnergy
  local valueLowering = 1
  if jefconfig.turbineBucketMode == "B" then
    valueLowering = 1000
  end

  bigfont.writeOn(choosenMonitor, 1, "Remaining \149 Capacity \149 Production", nil, 2)
  bigfont.writeOn(choosenMonitor, 1, string.rep("\131", 35), nil, 5)

  for k, turbine in pairs(message) do
    -- Ignore the message type key and first value
    if type(k) == "number" then
      -- Modify string for even spacing
      local steamAmount = turbine["steam"]
      if type(steamAmount) == "table" then steamAmount = steamAmount.amount end

      local availableArray = {tostring(math.floor(steamAmount / valueLowering))}
      local availableArrayAdjusted = {}

      local capacityArray = {tostring(math.floor(turbine["steamCap"]  / valueLowering))}
      local capacityArrayAdjusted = {}

      local neededArray = {tostring(math.floor(turbine["productionRate"] * jefconfig.energyTable[energyType])) .. " " .. energyType .. "/t"}
      local neededArrayAdjusted = {}

      -- Adjust the strings based on the length they should be.
      for k, v in pairs(availableArray) do
        availableArrayAdjusted[k] = jefutils.adjustStringSize(tostring(v), #"Available", "left")
      end

      for k, v in pairs(capacityArray) do
        capacityArrayAdjusted[k] = jefutils.adjustStringSize(tostring(v), #"Capacity", "left")
      end

      for k, v in pairs(neededArray) do
        neededArrayAdjusted[k] = jefutils.adjustStringSize(tostring(v), #"Production", "left")
      end

      -- Array elements choosen in the strings are a bit mixed since sometimes needed is actually cap and so on.
      steamString = availableArrayAdjusted[1] .. " \149 " .. capacityArrayAdjusted[1] .. " \149 " .. neededArrayAdjusted[1]
      steamColor = string.rep("8", #availableArrayAdjusted[1]) .. "000" .. string.rep("8", #capacityArrayAdjusted[1]) .. "000" .. string.rep("8", #neededArrayAdjusted[1])

      bigfont.blitOn(choosenMonitor, 1, steamString, steamColor, string.rep("f", #steamString), nil, 7 + ((k-1)*5))

      -- Draw bar and boiler number
      bigfont.writeOn(choosenMonitor, 1, "T." .. k, 10, 7 + ((k-1)*5))
      bigfont.writeOn(choosenMonitor, 1, string.rep("\131", #("B." .. k)+4), 9, 10 + ((k-1)*5))
      bigfont.writeOn(choosenMonitor, 1, string.rep("\131", 35), nil, 10 + ((k-1)*5))
    end
  end

  -- Draw the measurement unit info
  local unitString = "(" .. jefconfig.turbineBucketMode .. " Mode)"
  bigfont.blitOn(choosenMonitor, 1, unitString, string.rep("8", #unitString), string.rep("f", #unitString), nil, 7 + ((#message)*5))
end



b.drawSetupInfoMonitor = function(choosenMonitor, message)
  choosenMonitor.clear()

  for k, turbine in pairs(message) do
    -- Ignore the message type key and first value
    if type(k) == "number" then
      -- Modify title string to look better
      local titleArray = {"Condensers: ", "Dispersers: ", "Blades: ",
                          "Vents: ", "Coils: ", "Max Water Output: ",
                          "Max Flowrate: ", "Flowrate: "}
      local titleArraySize = {#titleArray[1], #titleArray[2], #titleArray[3], #titleArray[4],
                              #titleArray[5], #titleArray[6], #titleArray[7], #titleArray[8]}
      local titleMaxLength = math.max(unpack(titleArraySize))
      local titleArrayAdjusted = {}

      for k, v in pairs(titleArray) do
        v = string.sub(v, 1, -2)
        titleArrayAdjusted[k] = jefutils.adjustStringSize(tostring(v), titleMaxLength, "right")
      end

      -- Modify the full string to be placed center screen
      local infoArray = {"\149 " .. titleArrayAdjusted[1] .. turbine["condensers"],
                          "\149 " .. titleArrayAdjusted[2] .. turbine["dispersers"],
                          "\149 " .. titleArrayAdjusted[3] .. turbine["blades"],
                          "\149 " .. titleArrayAdjusted[4] .. turbine["vents"],
                          "\149 " .. titleArrayAdjusted[5] .. turbine["coils"],
                          "\149 " .. titleArrayAdjusted[6] .. turbine["maxWaterOutput"] .. " mB/t",
                          "\149 " .. titleArrayAdjusted[7] .. turbine["maxFlowRate"] .. " mB/t",
                          "\149 " .. titleArrayAdjusted[8] .. turbine["flowRate"] .. " mB/t"}
      local infoArraySize = {#infoArray[1], #infoArray[2], #infoArray[3], #infoArray[4],
                              #infoArray[5], #infoArray[6], #infoArray[7], #infoArray[8]}
      local infoMaxLength = math.max(unpack(infoArraySize))
      local infoArrayAdjusted = {}

      -- Draw bar and boiler number
      bigfont.writeOn(choosenMonitor, 1, "T." .. k, 8, 23 + ((k-1)*27))
      bigfont.writeOn(choosenMonitor, 1, string.rep("\131", #("T." .. k)+4), 6, ((k)*27))
      bigfont.writeOn(choosenMonitor, 1, string.rep("\131", infoMaxLength), nil, (k*27))

      for k2, v in pairs(infoArray) do
        adjustedValue = jefutils.adjustStringSize(tostring(v), infoMaxLength, "right")
        bigfont.writeOn(choosenMonitor, 1, adjustedValue, nil, 2 + ((k2-1) * 3) + ((k-1) * 27))
      end
    end
  end
end

return b
