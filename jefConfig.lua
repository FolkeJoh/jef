--Choose what monitor number each system should use
-- (numbers depend on your setup)
config = {}

-- Disable certain parts of J.E.F
config.enableReactor = true
config.enableECS = true
config.enableBoiler = true
config.enableTurbine = true
config.enableInduction = true

-- Change what monitor each output should use
config.monitorReactorRuntime = "monitor_17"
config.monitorReactorECS = "monitor_18"
config.monitorReactorSetup = "monitor_19"
config.monitorBoilerMain = "monitor_21"
config.monitorBoilerRuntime = "monitor_22"
config.monitorBoilerSetup = "monitor_20"
config.monitorTurbineMain = "monitor_24"
config.monitorTurbineRuntime = "monitor_23"
config.monitorTurbineSetup = "monitor_25"
config.monitorInductionMain = "monitor_27"

-- Change liquid/gas property details (mB / B)
config.reactorBucketMode = "B"
config.boilerBucketMode = "B"
config.turbineBucketMode = "B"

-- Change energy property details (J, FE, EU, RF, T)
-- Original energy form is in Joules (J)
config.turbineEnergy = "J"
config.inductionEnergy = "J"

-- Modify if needed. Values are gotten from:
-- Uses https://gist.github.com/DeflatedPickle/403e1eb0bb0bed7f2509142e63630726
config.energyTable = {
  J = 1,
  FE = 0.4,
  EU = 0.1,
  RF = 0.4,
  T = 0.4
}

return config
