-- Init
local b = {}
local bigfont = require("bigfont")
local jefutils = require("jefUtils")
local jefconfig = require("jefConfig")

-- Default Values
local timerX = 2
local timerY = 52

-- Bar placement
local barPadding = 24
local barHeight = 23
local barY = 43

-- config Values

b.drawTimer = function(choosenMonitor, lastTime, expectedTime)
    local timeDif = math.floor(os.clock()-lastTime)
    local timerString = "Last Update: " .. timeDif .. " Seconds"
    local timerColor = string.rep("d", #timerString)  -- Default green

    if timeDif-1 > expectedTime then              -- Turn orange
      timerColor = string.rep("1", #timerString)
    end
    if timeDif > expectedTime*2+1 then           -- Trun red
      timerColor = string.rep("e", #timerString)
    end

    choosenMonitor.setCursorPos(timerX, timerY)
    choosenMonitor.blit(timerString, timerColor, string.rep("f", #timerString))
end

b.drawMainMonitor = function(choosenMonitor, message, lastTime, expectedTime)
  -- For placement
  local monitorWidth, monitorHeight = choosenMonitor.getSize()

  -- Clear monitor
  choosenMonitor.clear()

  for k, v in pairs(message) do
    -- Ignore the message type key and first value
    if type(k) == "number" then
      local boilerString = "B." .. k

      if k == 1 then
        jefutils.drawBoiler(choosenMonitor, 20, 13, v, boilerString)
      else
        jefutils.drawBoiler(choosenMonitor, (73*(k-1)) + 20, 13, v, boilerString)
      end
    end
  end
end

b.drawRuntimeInfoMonitor = function(choosenMonitor, message)
  choosenMonitor.clear()

  local width, height = choosenMonitor.getSize()
  local valueLowering = 1
  if jefconfig.boilerBucketMode == "B" then
    valueLowering = 1000
  end

  bigfont.writeOn(choosenMonitor, 1, "Available \149 Capacity \149 Remaining", nil, 2)
  bigfont.writeOn(choosenMonitor, 1, string.rep("\131", 32), nil, 5)

  for k, boiler in pairs(message) do
    -- Ignore the message type key and first value
    if type(k) == "number" then
      -- Modify string for even spacing
      local coolantAmount = boiler["cooledCoolant"]
      local heatedAmount = boiler["heatedCoolant"]
      local waterAmount = boiler["water"]
      local steamAmount = boiler["steam"]

      if type(coolantAmount) == "table" then coolantAmount = coolantAmount.amount end
      if type(heatedAmount) == "table" then heatedAmount = heatedAmount.amount end
      if type(waterAmount) == "table" then waterAmount = waterAmount.amount end
      if type(steamAmount) == "table" then steamAmount = steamAmount.amount end

      local availableArray = {tostring(math.floor(coolantAmount / valueLowering)),
                              tostring(math.floor(heatedAmount / valueLowering)),
                              tostring(math.floor(waterAmount / valueLowering)),
                              tostring(math.floor(steamAmount / valueLowering))}
      local availableArrayAdjusted = {}

      local capacityArray = {tostring(math.floor(boiler["cooledCoolantCap"]  / valueLowering)),
                              tostring(math.floor(boiler["heatedCoolantCap"] / valueLowering)),
                              tostring(math.floor(boiler["waterCap"] / valueLowering)),
                              tostring(math.floor(boiler["steamCap"]  / valueLowering))}
      local capacityArrayAdjusted = {}

      local neededArray = {tostring(math.floor(boiler["cooledCoolantNeeded"]  / valueLowering)),
                              tostring(math.floor(boiler["heatedCoolantNeeded"] / valueLowering)),
                              "", ""}
      local neededArrayAdjusted = {}

      -- Adjust the strings based on the length they should be.
      for k, v in pairs(availableArray) do
        availableArrayAdjusted[k] = jefutils.adjustStringSize(tostring(v), #"Available", "left")
      end

      for k, v in pairs(capacityArray) do
        capacityArrayAdjusted[k] = jefutils.adjustStringSize(tostring(v), #"Capacity", "left")
      end

      for k, v in pairs(neededArray) do
        neededArrayAdjusted[k] = jefutils.adjustStringSize(tostring(v), #"Remaining", "left")
      end

      -- Array elements choosen in the strings are a bit mixed since sometimes needed is actually cap and so on.
      cooledCoolantString = availableArrayAdjusted[1] .. " \149 " .. capacityArrayAdjusted[1] .. " \149 " .. neededArrayAdjusted[1]
      cooledCoolantColor = string.rep("2", #availableArrayAdjusted[1]) .. "000" .. string.rep("2", #capacityArrayAdjusted[1]) .. "000" .. string.rep("2", #neededArrayAdjusted[1])

      heatedCoolantString = availableArrayAdjusted[2] .. " \149 " .. capacityArrayAdjusted[2] .. " \149 " .. neededArrayAdjusted[2]
      heatedCoolantColor = string.rep("e", #availableArrayAdjusted[2]) .. "000" .. string.rep("e", #capacityArrayAdjusted[2]) .. "000" .. string.rep("e", #neededArrayAdjusted[2])

      waterString = availableArrayAdjusted[3] .. " \149 " .. capacityArrayAdjusted[4] .. " \149 " .. string.rep(" ", #"Remaining")
      waterColor = string.rep("b", #availableArrayAdjusted[3]) .. "000" .. string.rep("b", #capacityArrayAdjusted[3]) .. "000" .. string.rep("b", #neededArrayAdjusted[3])

      steamString = availableArrayAdjusted[4] .. " \149 " .. capacityArrayAdjusted[4] .. " \149 " .. string.rep(" ", #"Remaining")
      steamColor = string.rep("8", #availableArrayAdjusted[4]) .. "000" .. string.rep("8", #capacityArrayAdjusted[4]) .. "000" .. string.rep("8", #neededArrayAdjusted[4])

      bigfont.blitOn(choosenMonitor, 1, cooledCoolantString, cooledCoolantColor, string.rep("f", #cooledCoolantString), nil, 7 + ((k-1)*14))
      bigfont.blitOn(choosenMonitor, 1, heatedCoolantString, heatedCoolantColor, string.rep("f", #heatedCoolantString), nil, 10 + ((k-1)*14))
      bigfont.blitOn(choosenMonitor, 1, waterString, waterColor, string.rep("f", #waterString), nil, 13 + ((k-1)*14))
      bigfont.blitOn(choosenMonitor, 1, steamString, steamColor, string.rep("f", #steamString), nil, 16 + ((k-1)*14))

      -- Draw bar and boiler number
      bigfont.writeOn(choosenMonitor, 1, "B." .. k, 4, 15 + ((k-1)*14))
      bigfont.writeOn(choosenMonitor, 1, string.rep("\131", #("B." .. k)), 4, 19 + ((k-1)*14))
      bigfont.writeOn(choosenMonitor, 1, string.rep("\131", 32), nil, 19 + ((k-1)*14))
    end
  end

  -- Draw the measurement unit info
  local unitString = "(" .. jefconfig.boilerBucketMode .. " Mode)"
  bigfont.blitOn(choosenMonitor, 1, unitString, string.rep("8", #unitString), string.rep("f", #unitString), nil, 7 + ((#message)*14))
end

b.drawSetupInfoMonitor = function(choosenMonitor, message)
  choosenMonitor.clear()

  bigfont.writeOn(choosenMonitor, 1, "Specifications", nil, 2)
  bigfont.writeOn(choosenMonitor, 1, string.rep("\131", #"Specifications"), nil, 5)

  for k, boiler in pairs(message) do
    -- Ignore the message type key and first value
    if type(k) == "number" then
      -- Modify title string to look better
      local titleArray = {"Max Boil Capacity: ", "Max Boilrate: ", "Environmental Loss: ",
                          "Superheaters: "}
      local titleArraySize = {#titleArray[1], #titleArray[2], #titleArray[3], #titleArray[4]}
      local titleMaxLength = math.max(unpack(titleArraySize))
      local titleArrayAdjusted = {}

      for k, v in pairs(titleArray) do
        v = string.sub(v, 1, -2)
        titleArrayAdjusted[k] = jefutils.adjustStringSize(tostring(v), titleMaxLength, "right")
      end

      -- Modify the full string to be placed center screen
      local infoArray = {"\149 " .. titleArrayAdjusted[1] .. boiler["boilCap"] .. "mB/t",
                          "\149 " .. titleArrayAdjusted[2] .. boiler["maxBoilRate"] .. " mB/t",
                          "\149 " .. titleArrayAdjusted[3] .. math.floor(boiler["environmentalLoss"]) .. " K/t",
                          "\149 " .. titleArrayAdjusted[4] .. boiler["superheaters"]}
      local infoArraySize = {#infoArray[1], #infoArray[2], #infoArray[3], #infoArray[4]}
      local infoMaxLength = math.max(unpack(infoArraySize))
      local infoArrayAdjusted = {}

      for k2, v in pairs(infoArray) do
        adjustedValue = jefutils.adjustStringSize(tostring(v), infoMaxLength, "right")
        bigfont.writeOn(choosenMonitor, 1, adjustedValue, nil, 7 + ((k2-1) * 3) + ((k-1) * 14))
      end

      -- Draw bar and boiler number
      bigfont.writeOn(choosenMonitor, 1, "B." .. k, 8, 15 + ((k-1)*14))
      bigfont.writeOn(choosenMonitor, 1, string.rep("\131", #("B." .. k)+4), 6, 19 + ((k-1)*14))
      bigfont.writeOn(choosenMonitor, 1, string.rep("\131", infoMaxLength), nil, 19 + ((k-1)*14))
    end
  end
end

return b
