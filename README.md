# J.E.F - Just Enough Fission

J.E.F is a CC script that allows for Mekanism's fission reactor system to be controlled and monitored remotely.
It is currently in its early stages so bug reports are happily taken here.

![Front Image 1 Missing](img/front1.png "Front Image")
![Front Image 2 Missing](img/front2.png "Front Image")
![Front Image 3 Missing](img/front3.png "Front Image")
![Front Image 4 Missing](img/front4.png "Front Image")

The information  below is due to change so if you're upgrading to a newer J.E.F version
please read through the sections. If the program gets stuck you can hold Ctrl + T to force a shutdown of J.E.F

## Requirements
These mods are needed with *advanced peripherals* depending on the *Mekanism* version you're using.
* Mekanism (V.10.1+ does not require *Advanced Peripherals*)
```https://www.curseforge.com/minecraft/mc-mods/mekanism```
* ComputerCraft: Tweaked
```https://www.curseforge.com/minecraft/mc-mods/cc-tweaked```
* Advanced Peripherals
```https://www.curseforge.com/minecraft/mc-mods/advanced-peripherals```

## Features
##### General
- Command interface to control the reactor.
- Timer displaying of listener is not responding.
- ECS (Emergency Control System) to manage and auto-SCRAM reactor.
- Config file to setup systems like monitors.
##### Reactor
- Display general information about the reactor (Coolant, Heated Coolant, Fuel, Waste) graphically.
- Display static reactor setup information such as number of fuel rod assemblies.
- Display (super) detailed values of reactor resources.
- Display critical information (Damage, Burnrate, Temperature).
##### Boilers
- Display general information about the boilers (Boilrate, Temperature, Status) graphically.
- Display static boiler setup information such as environmental loss and amount of super-heaters.
- Display (super) detailed values of boiler resources.
##### Turbines
- Display general information about the turbines (Dumping mode, Status) graphically.
- Display static turbine setup information such as flowrate and amount of vents.
- Display (super) detailed values of turbine resources.
##### Induction Matrix
- Display current energy stored, max energy storage, input and output in a graphicall way.
- Display static induction-matrix information such as amount of cells and transfer capacity.

## Commands
- start (starts the reactor at current burnrate).
- stop (stops the reactor, same as pressing SCRAM on reactor GUI)
- burnrate {number} (sets the reactors burnrate)
- exit (exits J.E.F by shutting down the reactor and clearing all monitors)
- setup monitor (display all the names of the monitors, requires restart of J.E.F)

## Setup
Feel free to contact me on discord (Korvbagarens#1361) if you need further help with the setup
after reading the following section.

### Setup - Control Computer
![Setup Controll Image Missing](img/setupControll.png "Setup Controll Image")
Place a computer as shown in the image with more screens connected the same way to display the other monitoring sections:
* An ender modem to one of its sides.
* A monitor directly adjacent to it (preferably above).
* A wired modem that's connected to the monitors shown on the image.

Note: Be sure to right click the wired modems on the monitors to activate them, the monitor directly
above the computer should NOT be connected through a wired modem.

Right click the computer and type in the following (press enter after each row):
```
pastebin get https://pastebin.com/EaeGeGvE installer
installer
jef
```
When starting the program in the future simply type:
```
jef
```
Or if you want the program to start everytime the server starts do:
```
rename jef startup
startup
```
### Setup - Listening Computers
The current supported structures include: Fission Reactor, Thermoelectric Boiler, Industrial Turbine and the Induction Matrix
![Setup Listener Image Missing](img/setupListener.png "Setup Listener Image")
Place a computer as shown in the image for every listening section (one for reactor, one for boilers, one for turbines and so on...):
* An ender modem to one of its sides.
* A wired modem that's connected to the blocks shown on the image.

Note: Be sure to right click the wired modems on the listening blocks to activate them.
The image also contains what blocks are the listening blocks for each listening section, this is important as
J.E.F can not get the data from the structure otherwise.

Right click the computer and type in the following (press enter after each row):
```
pastebin get https://pastebin.com/XchEM7we startup
startup
```
When starting the program simply type (program will auto-start on server startup if named "startup"):
```
startup
```

## Special Thanks!
Some special thanks to *Srendi* for making the development and intergration with advanced peripherals possible.
Another big thank to *Alien* for testing J.E.F in its early stages!

## Extra Notes
This code is free to use, modify and distribute under the condition specified in the disclaimer. (Found in main file)
