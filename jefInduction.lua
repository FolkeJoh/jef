-- Init
local b = {}
local bigfont = require("bigfont")
local jefutils = require("jefutils")
local jefconfig = require("jefConfig")

-- Default Values
local timerX = 140
local timerY = 52

b.drawTimer = function(choosenMonitor, lastTime, expectedTime)
    local timeDif = math.floor(os.clock()-lastTime)
    local timerString = "Last Update: " .. timeDif .. " Seconds"
    local timerColor = string.rep("d", #timerString)  -- Default green

    if timeDif-1 > expectedTime then              -- Turn orange
      timerColor = string.rep("1", #timerString)
    end
    if timeDif > expectedTime*2+1 then           -- Trun red
      timerColor = string.rep("e", #timerString)
    end

    choosenMonitor.setCursorPos(timerX, timerY)
    choosenMonitor.blit(timerString, timerColor, string.rep("f", #timerString))
end



b.drawMainMonitor = function(choosenMonitor, message, lastTime, expectedTime)
  -- For placement
  local monitorWidth, monitorHeight = choosenMonitor.getSize()

  -- Clear monitor
  choosenMonitor.clear()

  for k, induction in pairs(message) do
    -- Ignore the message type key and first value
    if type(k) == "number" then
      jefutils.drawInduction(choosenMonitor, 45, 10, 80, 25, induction)

      bigfont.writeOn(choosenMonitor, 1, "Cells: " .. induction["cells"], 2, monitorHeight-9)
      bigfont.writeOn(choosenMonitor, 1, "Providers: " .. induction["providers"], 2, monitorHeight-6)
      bigfont.writeOn(choosenMonitor, 1, "Transfer Capacity: " .. induction["transferCap"], 2, monitorHeight-3)
    end
  end
end

return b
