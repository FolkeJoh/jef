local devMode = false       -- Set to true if you want to use the experimental version
local updateConfig = true   -- Set to false if you don't want your config to change

local files = {}
if devMode then
  files["jef"] = "H1jeW7ET"
  files["jefReactor"] = "ZQfjLqdt"
  files["jefUtils"] = "BwBYBVM3"
  files["jefBoiler"] = "R6SbP80B"
  files["jefTurbine"] = "LTaik4zf"
  files["bigFont"] = "3LfWxRWh"
  if updateConfig then
    files["jefConfig"] = "dLmMxH6p"
  end
  files["jefInduction"] = "H0nBpmYj"
else
  files["jef"] = "aQuUdnLG"
  files["jefReactor"] = "XFfiYn49"
  files["jefUtils"] = "cm0UcpUc"
  files["jefBoiler"] = "3Cgy85qz"
  files["jefTurbine"] = "CyJAZ2b6"
  files["bigFont"] = "3LfWxRWh"
  files["jefInduction"] = "TD2bAu8p"
  if updateConfig then
    files["jefConfig"] = "4zTa5XxH"
  end
end

for k, v in pairs(files) do
  if fs.exists(k) then
    fs.delete(k)
  end

  local response = http.get("https://www.pastebin.com/raw/" .. v)
  local code = response.readAll()
  io.open(k, "w"):write(code):close()
end
