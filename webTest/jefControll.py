#!/usr/bin/env python3

import socket
import errno
from threading import Thread

CLIENT_HOST = ''
COMPUTER_HOST = ''
CLIENT_PORT = 6789
COMPUTER_PORT = 12345
BUFFER_SIZE = 4096
UNICODE = 'utf-8'

clientConnections = {}

class newConnection(Thread):
    def __init__(self, conn, addr, type):
        Thread.__init__(self)
        self.conn = conn
        self.addr = addr
        self.type = type

    def run(self):
        dataRecieved = self.conn.recv(1024).decode(UNICODE)
        print(dataRecieved)
        if self.type == "client":
            clientConnections[dataRecieved] = (self.conn, self.addr)
        elif self.type == "computer":
            print("Sending Data to: " , clientConnections[dataRecieved][0] , " | " , clientConnections[dataRecieved][1])
            # Send data to clientConnections[dataRecieved]!
        self.conn.close()

class listenThread(Thread):
    def __init__(self, host, port, type):
        Thread.__init__(self)
        self.host = host
        self.port = port
        self.type = type

    def run(self):
        listeningSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        listeningSock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        listeningSock.bind((self.host, self.port))
        listeningSock.listen(50)

        # Continuesly listen for connections
        while True:
            print('Listening for new Connections...')
            conn, addr = listeningSock.accept()
            try:
                print('Connection Received, Type: ' , self.type)
                newCon = newConnection(conn, addr, self.type)
                newCon.start()
            except socket.error as err:
                print('Something went wrong establishing connection!')
                print('Error: ', err)

        # Close listening socket
        listeningSock.close()

def main(clientHost, clientPort, computerHost, computerPort):
    listenClientThread = listenThread(clientHost, clientPort, "client")
    listenClientThread.start()

    listenComputerThread = listenThread(computerHost, computerPort, "computer")
    listenComputerThread.start()

main(CLIENT_HOST, CLIENT_PORT, COMPUTER_HOST, COMPUTER_PORT)
