local handle, err, errh = http.get("http://194.37.82.248:12347/")

if handle then
  print("Got connection: " .. handle.getResponseCode())
  print(handle.readAll())
  handle.close()
else
  print("Error: " .. err)
  if errh then
    print("Response: " .. errh.getResponseCode())
    print(errh.readAll())
    errh.close()
  end
end
