-- Init
local b = {}
local bigfont = require("bigfont")
local jefutils = require("jefUtils")
local jefconfig = require("jefConfig")

-- Default Values
local reactorName = "Main Reactor"
local timerX = 2
local timerY = 52

-- Bar placement
local barPadding = 24
local barHeight = 23
local barY = 43

-- Runtime variables
local lastECSPrint = ""

b.drawTimer = function(choosenMonitor, lastTime, expectedTime)
    local timeDif = math.floor(os.clock()-lastTime)
    local timerString = "Last Update: " .. timeDif .. " Seconds"
    local timerColor = string.rep("d", #timerString)  -- Default green

    if timeDif-1 > expectedTime then              -- Turn orange
      timerColor = string.rep("1", #timerString)
    end
    if timeDif > expectedTime*2+1 then           -- Turn red
      timerColor = string.rep("e", #timerString)
    end

    choosenMonitor.setCursorPos(timerX, timerY)
    choosenMonitor.blit(timerString, timerColor, string.rep("f", #timerString))
end

b.drawMainMonitor = function(choosenMonitor, message, lastTime, expectedTime)
  -- For placement
  monitorWidth, monitorHeight = choosenMonitor.getSize()

  -- Clear monitor
  choosenMonitor.clear()

  -- Draw title
  bigfont.writeOn(choosenMonitor, 2, reactorName, nil, 2)
  bigfont.writeOn(choosenMonitor, 1, string.rep("\131", #reactorName * 3), nil, 10)

  -- Draw status
  local statusString = "Status: " .. message["status"]
  local statusColor = string.rep("5", #statusString)
  local backgroundColor = string.rep("f", #statusString)
  bigfont.blitOn(choosenMonitor, 1, statusString, statusColor, backgroundColor, nil, barY-barHeight-7)

  -- Draw the bars
  local center = monitorWidth/2
  local coolantName = string.match(message["coolant"].name, ":(.*)"):gsub("^%l", string.upper)
  local coolantColor = "b"
  if coolantName == nil then
    coolantName = "Coolant"
  end
  if coolantName == "Sodium" then
    coolantColor = "2"
  end

  jefutils.drawPercentageBar(choosenMonitor, message["coolantFilledPercentage"], center-(barPadding*1.5), barY, barHeight, string.rep(coolantColor, 4), coolantName) -- Coolant
  jefutils.drawPercentageBar(choosenMonitor, message["heatedFilledPercentage"], center-(barPadding*0.5), barY, barHeight, "eeee", "Heated") -- Heated Coolant
  jefutils.drawPercentageBar(choosenMonitor, message["fuelFilledPercentage"], center+(barPadding*0.5), barY, barHeight, "5555", "Fuel") -- Fuel
  jefutils.drawPercentageBar(choosenMonitor, message["wasteFilledPercentage"], center+(barPadding*1.5), barY, barHeight, "cccc", "Waste") -- Waste
end



b.drawRuntimeInfoMonitor = function(choosenMonitor, message)
  choosenMonitor.clear()

  local width, height = choosenMonitor.getSize()
  local valueLowering = 1
  if jefconfig.reactorBucketMode == "B" then
    valueLowering = 1000
  end

  -- Draw bar (needs to be in background)
  bigfont.writeOn(choosenMonitor, 1, string.rep("\131", 32), nil, 19)

  bigfont.writeOn(choosenMonitor, 1, "Available \149 Capacity \149 Required", nil, 2)
  bigfont.writeOn(choosenMonitor, 1, string.rep("\131", 32), nil, 5)

  -- Modify string for even spacing
  local coolantAmount = message["coolant"]
  local heatedAmount = message["heated"]
  local fuelAmount = message["fuel"]
  local wasteAmount = message["waste"]
  
  if type(coolantAmount) == "table" then coolantAmount = coolantAmount.amount end
  if type(heatedAmount) == "table" then heatedAmount = heatedAmount.amount end
  if type(fuelAmount) == "table" then fuelAmount = fuelAmount.amount end
  if type(wasteAmount) == "table" then wasteAmount = wasteAmount.amount end

  local availableArray = {tostring(math.floor(coolantAmount / valueLowering)),
                          tostring(math.floor(heatedAmount / valueLowering)),
                          tostring(math.floor(fuelAmount / valueLowering)),
                          tostring(math.floor(wasteAmount / valueLowering))}
  local availableArrayAdjusted = {}

  local capacityArray = {tostring(math.floor(message["coolantCap"]  / valueLowering)),
                          tostring(math.floor(message["heatedCap"] / valueLowering)),
                          tostring(math.floor(message["fuelCap"] / valueLowering)),
                          tostring(math.floor(message["wasteCap"]  / valueLowering))}
  local capacityArrayAdjusted = {}

  local neededArray = {tostring(math.floor(message["coolantNeeded"]  / valueLowering)),
                          tostring(math.floor(message["heatedNeeded"] / valueLowering)),
                          tostring(math.floor(message["fuelNeeded"] / valueLowering)),
                          tostring(math.floor(message["wasteNeeded"]  / valueLowering))}
  local neededArrayAdjusted = {}

  -- Adjust the strings based on the length they should be.
  for k, v in pairs(availableArray) do
    availableArrayAdjusted[k] = jefutils.adjustStringSize(tostring(v), #"Available", "left")
  end

  for k, v in pairs(capacityArray) do
    capacityArrayAdjusted[k] = jefutils.adjustStringSize(tostring(v), #"Capacity", "left")
  end

  for k, v in pairs(neededArray) do
    neededArrayAdjusted[k] = jefutils.adjustStringSize(tostring(v), #"Required", "left")
  end

  -- Array elements choosen in the strings are a bit mixed since sometimes needed is actually cap and so on.
  local coolantString = availableArrayAdjusted[1] .. " \149 " .. capacityArrayAdjusted[1] .. " \149 " .. neededArrayAdjusted[1]
  local cs = "b"
  if string.match(message["coolant"].name, ":(.*)") == "sodium" then
    cs = "2"
  end
  local coolantColor = string.rep(cs, #availableArrayAdjusted[1]) .. "000" .. string.rep(cs, #capacityArrayAdjusted[1]) .. "000" .. string.rep(cs, #neededArrayAdjusted[1])

  local heatedString = availableArrayAdjusted[2] .. " \149 " .. capacityArrayAdjusted[2] .. " \149 " .. string.rep(" ", #"Required")
  local heatedColor = string.rep("e", #availableArrayAdjusted[2]) .. "000" .. string.rep("e", #capacityArrayAdjusted[2]) .. "000" .. string.rep("e", #neededArrayAdjusted[2])

  local fuelString = availableArrayAdjusted[3] .. " \149 " .. capacityArrayAdjusted[4] .. " \149 " .. neededArrayAdjusted[3]
  local fuelColor = string.rep("5", #availableArrayAdjusted[3]) .. "000" .. string.rep("5", #capacityArrayAdjusted[3]) .. "000" .. string.rep("5", #neededArrayAdjusted[3])

  local wasteString = availableArrayAdjusted[4] .. " \149 " .. capacityArrayAdjusted[4] .. " \149 " .. string.rep(" ", #"Required")
  local wasteColor = string.rep("c", #availableArrayAdjusted[4]) .. "000" .. string.rep("c", #capacityArrayAdjusted[4]) .. "000" .. string.rep("c", #neededArrayAdjusted[4])

  bigfont.blitOn(choosenMonitor, 1, coolantString, coolantColor, string.rep("f", #coolantString), nil, 7)
  bigfont.blitOn(choosenMonitor, 1, heatedString, heatedColor, string.rep("f", #heatedString), nil, 10)
  bigfont.blitOn(choosenMonitor, 1, fuelString, fuelColor, string.rep("f", #fuelString), nil, 13)
  bigfont.blitOn(choosenMonitor, 1, wasteString, wasteColor, string.rep("f", #wasteString), nil, 16)

  -- Draw the measurement unit info
  local unitString = "(" .. jefconfig.reactorBucketMode .. " Mode)"
  bigfont.blitOn(choosenMonitor, 1, unitString, string.rep("8", #unitString), string.rep("f", #unitString), nil, 21)

  -- Draw the extra info
  bigfont.writeOn(choosenMonitor, 1, "Heating Rate: " .. message["heatingRate"] .. " J/K", 2, 33)
  bigfont.writeOn(choosenMonitor, 1, "Environmental Loss: " .. string.format("%.4f", message["environmentalLoss"]) .. " K/t", 2, 36)
end



b.drawSetupInfoMonitor = function(choosenMonitor, message, JEFversion, JEFcolor, JEFslogan)
  choosenMonitor.clear()

  bigfont.writeOn(choosenMonitor, 1, "Specifications", nil, 2)
  bigfont.writeOn(choosenMonitor, 1, string.rep("\131", #"Specifications"), nil, 5)

  -- Modify title string to look better
  local titleArray = {"Maximum Burnrate: ", "Heat Capacity: ", "Fuel Assemblies: ",
                      "Fuel Surface Area: ", "Boil Efficiency: ", "Burnrate: "}
  local titleArraySize = {#titleArray[1], #titleArray[2], #titleArray[3], #titleArray[4], #titleArray[5]}
  local titleMaxLength = math.max(unpack(titleArraySize))
  local titleArrayAdjusted = {}

  for k, v in pairs(titleArray) do
    v = string.sub(v, 1, -2)
    titleArrayAdjusted[k] = jefutils.adjustStringSize(tostring(v), titleMaxLength, "right")
  end

  -- Modify the full string to be placed center screen
  local infoArray = {titleArrayAdjusted[1] .. message["maxBurnRate"] .. " mB/t",
                      titleArrayAdjusted[2] .. message["heatCapacity"] .. " J/K",
                      titleArrayAdjusted[3] .. message["fuelAssemblies"],
                      titleArrayAdjusted[4] .. message["fuelSurfaceArea"] .. " m\178",
                      titleArrayAdjusted[5] .. message["boilEfficiency"],
                      titleArrayAdjusted[6] .. message["burnRate"] .. " mB/t"}
  local infoArraySize = {#infoArray[1], #infoArray[2], #infoArray[3], #infoArray[4], #infoArray[5], #infoArray[6]}
  local infoMaxLength = math.max(unpack(infoArraySize))
  local infoArrayAdjusted = {}

  for k, v in pairs(infoArray) do
    adjustedValue = jefutils.adjustStringSize(tostring(v), infoMaxLength, "right")
    bigfont.writeOn(choosenMonitor, 1, adjustedValue, nil, 7 + ((k-1) * 3))
  end

  -- Write JEF version
  bigfont.blitOn(choosenMonitor, 1, JEFversion, JEFcolor, string.rep("f", #JEFversion), 2, 36)
end



-- draw the ES (Emergency Control System) monitor
b.drawECSMonitor = function(choosenMonitor, message)
  choosenMonitor.clear()

  local title = "Emergency Control System"

  local damage = message["damagePercent"]
  local damageString = string.format("%.4f", tostring(damage * 100)) .. " %"
  local damageColor = string.rep("d", #damageString)

  local temp = message["temperature"]
  local tempString = string.format("%.4f", tostring(temp)) .. " K"
  local tempColor = string.rep("d", #tempString)

  local burnRate = message["actualBurnRate"]
  local burnRateString = burnRate .. " mB/t"
  local burnRateColor = string.rep("0", #burnRateString)

  -- Modify colors
  if damage > 0 and damage < 0.2 then
    damageColor = string.rep("1", #damageString)  -- orange
  elseif damage >= 0.2 then
    damageColor = string.rep("e", #damageString)  -- Red
  end

  if temp > 600 and temp < 1000 then
    tempColor = string.rep("4", #tempString)  -- Yellow
  elseif temp > 1000 and temp < 1200 then
    tempColor = string.rep("1", #tempString)  -- Orange
  elseif temp >= 1200 then
    tempColor = string.rep("e", #tempString)  -- Red
  end

  bigfont.writeOn(choosenMonitor, 1, title, nil, 2)
  bigfont.writeOn(choosenMonitor, 1, string.rep("\131", #title), nil, 5)

  bigfont.blitOn(choosenMonitor, 1, damageString, damageColor, string.rep("f", #damageString), nil, 8)
  bigfont.blitOn(choosenMonitor, 1, tempString, tempColor, string.rep("f", #tempString), nil, 11)
  bigfont.blitOn(choosenMonitor, 1, burnRateString, burnRateColor, string.rep("f", #burnRateString), nil, 14)

  -- Draw last ECS action
  bigfont.writeOn(choosenMonitor, 1, lastECSPrint, nil, 21)
end



b.ECScheck = function(message, modem, modemChannel)
  -- Only SCRAM if active
  if message["status"] == "Active" then
    local SCRAM = {}

    -- If damage is over or equal 20%
    if message["damagePercent"] >= 0.2 then
      lastECSPrint = "Sent SCRAM Due to Damage Percentage"
      SCRAM["type"] = "SCRAM"
      SCRAM["context"] = "Damage Is To High: " .. message["damagePercent"]
    end
    -- If heat is over or equal 1200K
    if message["temperature"] >= 1200 then
      lastECSPrint = "Sent SCRAM Due to Temperature"
      SCRAM["type"] = "SCRAM"
      SCRAM["context"] = "Temp Is To High: " .. message["damagePercent"]
    end

    -- Send SCRAM
    if not (SCRAM["context"] == nil) then
      modem.transmit(modemChannel, modemChannel, SCRAM)
    end
  end
end

return b
